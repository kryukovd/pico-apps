// #define WOKWI             // Uncomment if running on Wokwi RP2040 emulator.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.
#include "pico/multicore.h" // Required for using multiple cores on the RP2040.

/**
 * @brief EXAMPLE - HELLO_C
 *        Simple example to initialise the IOs and then
 *        print a "Hello World!" message to the console.
 *
 * @return int  Application return code (zero for success).
 */

/**
 * @brief This function acts as the main entry-point for core #1.
 *        A function pointer is passed in via the FIFO with one
 *        incoming int32_t used as a parameter. The function will
 *        provide an int32_t return value by pushing it back on
 *        the FIFO, which also indicates that the result is ready.
 */

void core1_entry()
{
    while (1)
    {
        //// Function pointer is passed to us via the FIFO
        // We have one incoming int32_t as a parameter, and will provide an
        // int32_t return value by simply pushing it back on the FIFO
        // which also indicates the result is ready.
        int32_t (*func)() = (int32_t(*)())multicore_fifo_pop_blocking();
        int32_t p = multicore_fifo_pop_blocking();
        int32_t result = (*func)(p);
        multicore_fifo_push_blocking(result);
    }
}

#define FLAG_VALUE 123

// calculating float Pi value
float wallisF(int it)
{
    float out = 1;
    for (float n = 1; n <= it; n++)
    {
        out = out * (((2 * n) / (2 * n - 1)) * ((2 * n) / (2 * n + 1)));
    }
    return out * 2;
}
// calculating double Pi value
double wallisD(int it)
{
    double out = 1;
    for (double n = 1; n <= it; n++)
    {
        out = out * (((2 * n) / (2 * n - 1)) * ((2 * n) / (2 * n + 1)));
    }
    return out * 2;
}

int main()
{
    const int ITER_MAX = 100000;

    uint32_t tm1;
    uint32_t tm2;
    uint32_t tm;
    
    uint32_t sctm1;
    uint32_t sctm2;
    uint32_t sctm;

    stdio_init_all();

    
    printf("\n\n\n time test lab06 \n\n\n");


    // single core single precision
    sctm1 = time_us_32() / 1000;
    printf("calling the WallisF function \n");
    tm1 = time_us_32() / 1000;
    // printf("current time: %u \n", tm1);
    float p_f = wallisF(ITER_MAX);
    printf("float Pi value is %f \n", p_f);
    tm2 = time_us_32() / 1000;
    // printf("current time: %u \n", tm2);
    tm = (tm2 - tm1);
    printf("WallistF single core time: %u ms \n\n", tm);

    // single core double precision
    printf("calling the WallisD function \n");
    tm1 = time_us_32() / 1000;
    // printf("current time: %u \n", tm1);
    double p_d = wallisD(ITER_MAX);
    printf("double Pi value is %lf \n", p_d);
    tm2 = time_us_32() / 1000;
    // printf("current time: %u \n", tm2);
    tm = (tm2 - tm1);
    printf("WallistD single core time: %u ms\n\n", tm);

    // total time for single-core
    sctm2 = time_us_32() / 1000;
    sctm = (sctm2 - sctm1);
    printf("single core sequentially time: %u\n\n\n", sctm);
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    
    // double res;

    // This example dispatches arbitrary functions to run on the second core
    // To do this we run a dispatcher on the second core that accepts a function
    // pointer and runs it

    // dual-core part
    absolute_time_t time1 = get_absolute_time();


    // core1 start
    multicore_launch_core1(core1_entry);

    multicore_fifo_push_blocking((uintptr_t)&wallisD);
    multicore_fifo_push_blocking(ITER_MAX);

    // core0 function
    absolute_time_t dctime1 = get_absolute_time();
    float core1_res2 = wallisF(ITER_MAX);
    absolute_time_t dctime2 = get_absolute_time();

    // core1 output
    double core1_res = multicore_fifo_pop_blocking();
    absolute_time_t time2 = get_absolute_time();


    // timings

    uint64_t dctime = absolute_time_diff_us(dctime1, dctime2);
    printf("core0(float): full time: %f ms \n", (double)dctime / (double)1000);

    uint64_t time = absolute_time_diff_us(time1, time2);
    printf("core1(double): full time: %f ms \n", (double)time/(double)1000);

    printf("total time: %f \n\n", (double)time/(double)1000);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Print a console message to inform user what's going on.
    // printf("Hello World!\n");

    // float p_f = wallisF();
    // double p_d = wallisD();
    // double p_def = 3.14159265359;
    // printf("float Pi: %f \nfloat error: %lf\ndouble Pi: %lf \ndouble error: %lf\ndefault: %lf", p_f, fabs(p_def - p_f), p_d, fabs(p_def - p_d), p_def);

    // Returning zero indicates everything went okay.
    return 0;
}
