    // #define WOKWI             // Uncomment if running on Wokwi RP2040 emulator.

    #include <stdio.h>
    #include <stdlib.h>
    #include "pico/stdlib.h"
    #include "pico/float.h"
    #include "pico/double.h"
    /**
     * @brief EXAMPLE - HELLO_C
     *        Simple example to initialise the IOs and then
     *        print a "Hello World!" message to the console.
     *
     * @return int  Application return code (zero for success).
     */
    
    // calculating float Pi value
    float wallisF(){
        float out = 1;
        for (float n = 1; n <= 100000; n++){
            out = out * (((2 * n) / (2 * n - 1)) * ((2 * n) / (2 * n + 1)));
        }
            return out * 2;
    }
    // calculating double Pi value
    double wallisD(){
        double out = 1;
        for (double n = 1; n <= 100000; n++){
            out = out * (((2 * n) / (2 * n - 1)) * ((2 * n) / (2 * n + 1)));
        }
        return out * 2;
    }

    int main()
    {

    #ifndef WOKWI
    // #include "pico/stdlib.h"
    // #include "pico/float.h"
    // #include "pico/double.h"
        // Initialise the IO as we will be using the UART
        // Only required for hardware and not needed for Wokwi
        stdio_init_all();
    #endif

        // Print a console message to inform user what's going on.
        // printf("Hello World!\n");
        float p_f = wallisF();
        double p_d = wallisD();
        double p_def = 3.14159265359;
        printf("float Pi: %f \nfloat error: %lf\ndouble Pi: %lf \ndouble error: %lf\ndefault: %lf", p_f, fabs(p_def - p_f), p_d, fabs(p_def - p_d), p_def);
        // Returning zero indicates everything went okay.
        return 0;
    }
