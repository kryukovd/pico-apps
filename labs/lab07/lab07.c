// #define WOKWI             // Uncomment if running on Wokwi RP2040 emulator.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.
#include "pico/multicore.h" // Required for using multiple cores on the RP2040.

/**
 * @brief EXAMPLE - HELLO_C
 *        Simple example to initialise the IOs and then
 *        print a "Hello World!" message to the console.
 *
 * @return int  Application return code (zero for success).
 */

/**
 * @brief This function acts as the main entry-point for core #1.
 *        A function pointer is passed in via the FIFO with one
 *        incoming int32_t used as a parameter. The function will
 *        provide an int32_t return value by pushing it back on
 *        the FIFO, which also indicates that the result is ready.
 */

void core1_entry()
{
    while (1)
    {
        //// Function pointer is passed to us via the FIFO
        // We have one incoming int32_t as a parameter, and will provide an
        // int32_t return value by simply pushing it back on the FIFO
        // which also indicates the result is ready.
        int32_t (*func)() = (int32_t(*)())multicore_fifo_pop_blocking();
        int32_t p = multicore_fifo_pop_blocking();
        int32_t result = (*func)(p);
        multicore_fifo_push_blocking(result);
    }
}

#define FLAG_VALUE 123

// calculating float Pi value
float wallisF(int it)
{
    float out = 1;
    for (float n = 1; n <= it; n++)
    {
        out = out * (((2 * n) / (2 * n - 1)) * ((2 * n) / (2 * n + 1)));
    }
    return out * 2;
}
// calculating double Pi value
double wallisD(int it)
{
    double out = 1;
    for (double n = 1; n <= it; n++)
    {
        out = out * (((2 * n) / (2 * n - 1)) * ((2 * n) / (2 * n + 1)));
    }
    return out * 2;
}

bool get_xip_cache_en()
{
    uint32_t *base = (uint32_t *) 0x14000000;
    uint32_t res = 1;
    res = *base & res;
    if(res = 0x1){
        return 1;
    }
    else{
        return 0;
    }
}

bool set_xip_cache_en (bool enable){
    uint32_t *base = (uint32_t *) 0x14000000;
    uint32_t cache = 1;
    uint32_t cache0 = 0;
    if (enable)
    {
        *base |= cache;
        return true;
    }
    else
    {
        *base &= cache0;
        return false;
    }
}

int main()
{
    absolute_time_t time1, time2, dctime1, dctime2, timet1, timet2;
    uint64_t time, dctime, timet;
    const int ITER_MAX = 100000;
    float p_f;
    double p_d;
    float core1_res2;
    double core1_res;
    stdio_init_all();

    printf("\n\n\n ___________________ time test lab06 ___________________ \n\n\n");

    // absolute_time_t time1 = get_absolute_time();
    // absolute_time_t time2 = get_absolute_time();
    // uint64_t time = absolute_time_diff_us(time1, time2);

    // Function to get the enable status of the XIP cache
    // bool get_xip_cache_en();

    // Function to set the enable status of the XIP cache
    // bool set_xip_cache_en(bool cache_en);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    bool e = get_xip_cache_en();

    // Using a single CPU core with caches enabled
    set_xip_cache_en(true);

    // single core single precision
    printf("cache enabled \n \n calling the WallisF function \n");

     timet1 = get_absolute_time();

     time1 = get_absolute_time();
     p_f = wallisF(ITER_MAX);
     time2 = get_absolute_time();
    printf("float Pi value is %f \n", p_f);

     time = absolute_time_diff_us(time1, time2);
    printf("WallistF single core time: %f ms \n\n", (double)time / (double)1000);

    // single core double precision
    printf("calling the WallisD function \n");
     time1 = get_absolute_time();
     p_d = wallisD(ITER_MAX);
     time2 = get_absolute_time();
    printf("double Pi value is %lf \n", p_d);

     time = absolute_time_diff_us(time1, time2);
    printf("WallistD single core time: %f ms\n\n", (double)time / (double)1000);

     timet2 = get_absolute_time();
     timet = absolute_time_diff_us(timet1, timet2);
    printf("total time: %f \n\n", (double)timet / (double)1000);
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Using a single CPU core with caches disabled

    set_xip_cache_en(false);   

    // single core single precision
    printf("cache disabled \n \n calling the WallisF function \n");

     timet1 = get_absolute_time();

     time1 = get_absolute_time();
     p_f = wallisF(ITER_MAX);
     time2 = get_absolute_time();
    printf("float Pi value is %f \n", p_f);

     time = absolute_time_diff_us(time1, time2);
    printf("WallistF single core time: %f ms \n\n", (double)time / (double)1000);

    // single core double precision
    printf("calling the WallisD function \n");
     time1 = get_absolute_time();
     p_d = wallisD(ITER_MAX);
     time2 = get_absolute_time();
    printf("double Pi value is %lf \n", p_d);

     time = absolute_time_diff_us(time1, time2);
    printf("WallistD single core time: %f ms\n\n", (double)time / (double)1000);

     timet2 = get_absolute_time();
     timet = absolute_time_diff_us(timet1, timet2);
    printf("total time: %f \n\n", (double)timet / (double)1000);
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Using both CPU cores with caches enabled

    set_xip_cache_en(true);

    // double res;

    // This example dispatches arbitrary functions to run on the second core
    // To do this we run a dispatcher on the second core that accepts a function
    // pointer and runs it

    // dual-core part

    // core1 start
     timet1 = get_absolute_time();
    multicore_launch_core1(core1_entry);

     time1 = get_absolute_time();
    multicore_fifo_push_blocking((uintptr_t)&wallisD);
    multicore_fifo_push_blocking(ITER_MAX);

    // core0 function
     dctime1 = get_absolute_time();
     core1_res2 = wallisF(ITER_MAX);
     dctime2 = get_absolute_time();
    // core1 output
     core1_res = multicore_fifo_pop_blocking();
     time2 = get_absolute_time();

    // timings
     timet2 = get_absolute_time();

     time = absolute_time_diff_us(time1, time2);
     dctime = absolute_time_diff_us(dctime1, dctime2);
     timet = absolute_time_diff_us(timet1, timet2);
    printf("cache enabled \n \n core0(float): full time: %f ms \n", (double)dctime / (double)1000);
    printf("core1(double): full time: %f ms \n", (double)time / (double)1000);
    printf("total time: %f \n\n", (double)timet / (double)1000);

    // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // // Using both CPU cores with caches disabled

    set_xip_cache_en(false);

    // double res;

    // This example dispatches arbitrary functions to run on the second core
    // To do this we run a dispatcher on the second core that accepts a function
    // pointer and runs it

    // dual-core part

    // core1 start
     timet1 = get_absolute_time();
    multicore_launch_core1(core1_entry);

     time1 = get_absolute_time();
    multicore_fifo_push_blocking((uintptr_t)&wallisD);
    multicore_fifo_push_blocking(ITER_MAX);

    // core0 function
     dctime1 = get_absolute_time();
     core1_res2 = wallisF(ITER_MAX);
     dctime2 = get_absolute_time();
    // core1 output
     core1_res = multicore_fifo_pop_blocking();
     time2 = get_absolute_time();
    
    
    // timings
     timet2 = get_absolute_time();

     time = absolute_time_diff_us(time1, time2);
     dctime = absolute_time_diff_us(dctime1, dctime2);
     timet = absolute_time_diff_us(timet1, timet2);
    printf("cache disabled \n \n core0(float): full time: %f ms \n", (double)dctime / (double)1000);
    printf("core1(double): full time: %f ms \n", (double)time / (double)1000);
    printf("total time: %f \n\n", (double)timet / (double)1000);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Print a console message to inform user what's going on.
    // printf("Hello World!\n");

    // float p_f = wallisF();
    // double p_d = wallisD();
    // double p_def = 3.14159265359;
    // printf("float Pi: %f \nfloat error: %lf\ndouble Pi: %lf \ndouble error: %lf\ndefault: %lf", p_f, fabs(p_def - p_f), p_d, fabs(p_def - p_d), p_def);

    // Returning zero indicates everything went okay.
    return 0;
}
