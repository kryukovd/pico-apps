#include "pico/stdlib.h"

/**
 * @brief EXAMPLE - BLINK_C
 *        Simple example to initialise the built-in LED on
 *        the Raspberry Pi Pico and then flash it forever.
 *
 * @return int  Application return code (zero for success).
 */


void while_content(uint pin_led, uint delay_led) //!< while subroutine with all the contents. It lights the led and sleeps for a delay

{
    // Toggle the LED on and then sleep for delay period
    gpio_put(pin_led, 1);
    sleep_ms(delay_led);

    // Toggle the LED off and then sleep for delay period
    gpio_put(pin_led, 0);
    sleep_ms(delay_led);
}

int main()
{

    // Specify the PIN number and sleep delay
    const uint LED_PIN = 25;
    const uint LED_DELAY = 500;

    // Setup the LED pin as an output.
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

    // Do forever...
    while (true)
    {
        while_content(LED_PIN, LED_DELAY);
        // // Toggle the LED on and then sleep for delay period
        // gpio_put(LED_PIN, 1);
        // sleep_ms(LED_DELAY);

        // // Toggle the LED off and then sleep for delay period
        // gpio_put(LED_PIN, 0);
        // sleep_ms(LED_DELAY);
    }

    // Should never get here due to infinite while-loop.
    return 0;
}


