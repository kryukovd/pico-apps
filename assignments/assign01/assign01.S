#include "hardware/regs/addressmap.h"
#include "hardware/regs/io_bank0.h"
#include "hardware/regs/timer.h"
#include "hardware/regs/m0plus.h"

.syntax unified
.cpu    cortex-m0plus
.thumb
.global main_asm
.align  4

.equ    DFLT_STATE_STRT, 1            @ Specify the value to start flashing
.equ    DFLT_STATE_STOP, 0            @ Specify the value to stop flashing
.equ    DFLT_ALARM_TIME, 1000000      @ Specify the default alarm timeout

.equ    GPIO_BTN_DN_MSK, 0x00040000   @ Bit-18 for falling-edge event on GP20
.equ    GPIO_BTN_EN_MSK, 0x00400000   @ Bit-22 for falling-edge event on GP21
.equ    GPIO_BTN_UP_MSK, 0x04000000   @ Bit-26 for falling-edge event on GP22

.equ    GPIO_BTN_DN,  20              @ Specify pin for the "down" button
.equ    GPIO_BTN_EN,  21              @ Specify pin for the "enter" button
.equ    GPIO_BTN_UP,  22              @ Specify pin for the "up" button
.equ    GPIO_LED_PIN, 25              @ Specify pin for the built-in LED
.equ    GPIO_DIR_IN,   0              @ Specify input direction for a GPIO pin
.equ    GPIO_DIR_OUT,  1              @ Specify output direction for a GPIO pin

.equ    LED_VAL_ON,    1              @ Specify value that turns the LED "on"
.equ    LED_VAL_OFF,   0              @ Specify value that turns the LED "off"

.equ    GPIO_ISR_OFFSET, 0x74         @ GPIO is int #13 (vector table entry 29)
.equ    ALRM_ISR_OFFSET, 0x40         @ ALARM0 is int #0 (vector table entry 16)

@ Entry point to the ASM portion of the program
main_asm:
    bl init_gpio_led       @ Initialise the GPIO LED pin
    bl init_gpio_down @ Initialise the GPIO down pin
    bl init_gpio_enter @ Initialise the GPIO enter pin
    bl init_gpio_up @ Initialise the GPIO up pin

    bl    install_alarm_isr                      
    bl    install_gpio_isr                       
    

    ldr   r6, =DFLT_ALARM_TIME      @ register for the delay rate, which is later manipulated with            
    bl    set_alarm                              
    
    @ enabling the alarm
    ldr   r3, =(TIMER_BASE+TIMER_INTE_OFFSET)  
    movs  r1, #1                                  
    str   r1, [r3]      


main_loop:
    

    wfi     @ wait


    b       main_loop          



@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@



@ Subroutines used to initialise the PI Pico built-in LED and the buttons
init_gpio_led:
    push    {lr}   
    movs    r0, #GPIO_LED_PIN           @ This value is the GPIO LED pin on the PI PICO board
    bl      asm_gpio_init               @ Call the subroutine to initialise the GPIO pin specified by r0
    movs    r0, #GPIO_LED_PIN           @ This value is the GPIO LED pin on the PI PICO board
    movs    r1, #GPIO_DIR_OUT 
    bl      asm_gpio_set_dir            @ Call the subroutine to set the GPIO pin specified by r0 to state specified by r1
    pop     {pc}   

init_gpio_down:
    push    {lr}   
    movs    r0, #GPIO_BTN_DN           @ This value is the GPIO down pin on the PI PICO board
    bl      asm_gpio_init               @ Call the subroutine to initialise the GPIO pin specified by r0
    movs    r0, #GPIO_BTN_DN           @ This value is the GPIO down pin on the PI PICO board
    movs    r1, #GPIO_DIR_IN 
    bl      asm_gpio_set_dir            @ Call the subroutine to set the GPIO pin specified by r0 to state specified by r1

    movs r0, #GPIO_BTN_DN
    bl  asm_gpio_set_irq

    pop     {pc}   

init_gpio_enter:
    push    {lr}   
    movs    r0, #GPIO_BTN_EN           @ This value is the GPIO enter pin on the PI PICO board
    bl      asm_gpio_init               @ Call the subroutine to initialise the GPIO pin specified by r0
    movs    r0, #GPIO_BTN_EN           @ This value is the GPIO enter pin on the PI PICO board
    movs    r1, #GPIO_DIR_IN 
    bl      asm_gpio_set_dir            @ Call the subroutine to set the GPIO pin specified by r0 to state specified by r1

    movs r0, #GPIO_BTN_EN
    bl  asm_gpio_set_irq

    pop     {pc}   

init_gpio_up:
    push {lr}   
    movs r0, #GPIO_BTN_UP           @ This value is the GPIO up pin on the PI PICO board
    bl   asm_gpio_init               @ Call the subroutine to initialise the GPIO pin specified by r0
    movs r0, #GPIO_BTN_UP           @ This value is the GPIO up pin on the PI PICO board
    movs r1, #GPIO_DIR_IN 
    bl   asm_gpio_set_dir            @ Call the subroutine to set the GPIO pin specified by r0 to state specified by r1
    
    movs r0, #GPIO_BTN_UP
    bl  asm_gpio_set_irq

    pop     {pc}   



@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


@installing button handler

install_gpio_isr:
    ldr r3, =(PPB_BASE+M0PLUS_VTOR_OFFSET)
    @ add r3, r3, #M0PLUS_VTOR_OFFSET
    ldr r1, [r3]
    ldr r3, =GPIO_ISR_OFFSET
    add r3, r1
    ldr r0, =gpio_isr
    str r0, [r3]

    ldr r3, =(PPB_BASE+M0PLUS_NVIC_ICPR_OFFSET)
    @ add r3, r3, #M0PLUS_NVIC_ICPR_OFFSET
    ldr r0, =1
    lsls r0, #13
    str r0, [r3]

    ldr r3, =(PPB_BASE+M0PLUS_NVIC_ISER_OFFSET)
    @ add r3, r3, #M0PLUS_NVIC_ISER_OFFSET
    str r0, [r3]

    bx lr



@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

@ button handler    

@ this handler decides what to do with the led depending on the pressed button 
@ it reads the button event and compares it to masks
@ the current state of the flashing is saved in order to do a second check inside the 
@ up and down functions and to decide if it requires reset

.thumb_func
gpio_isr:
    push {lr}

    ldr  r3, =(IO_BANK0_BASE+IO_BANK0_PROC0_INTS2_OFFSET) @ get the button 
    Ldr  r1, [r3]   	                                   
    ldr r0, =1
    lsls r0, #13                                       
    str  r0, [r3]                                           
    ldr  r3, =(TIMER_BASE+TIMER_INTE_OFFSET)             
    ldr  r0, [r3]   @ get current state                                  
    ldr  r3, =GPIO_BTN_DN_MSK        
    @ decrease
    CMP  r3, r1                                            
    beq  down     

    ldr  r3, =GPIO_BTN_UP_MSK     
    @ increase
    CMP  r3, r1                                            
    beq  up     

    ldr  r3,=GPIO_BTN_EN_MSK   
    @ exception 
    CMP  r3, r1                                            
    bne  out              
    @ reset the button                            
    ldr  r1, =GPIO_BTN_EN_MSK                                      
    ldr  r3, =(IO_BANK0_BASE+IO_BANK0_INTR2_OFFSET)      
    str  r1, [r3]                                           
    ldr  r3,=DFLT_STATE_STOP           

    cmp  r0, r3                                            
    bne  turn_off 

    @ turning led on
    movs r1, #1                                             
    ldr  r3, =(TIMER_BASE+TIMER_INTE_OFFSET)             
    str  r1, [r3]                                          
    ldr  r0, =message2                                         
    bl   printf                                            
    bl   out                                               


down:
    @ reset the button
    ldr r3, =(IO_BANK0_BASE+IO_BANK0_INTR2_OFFSET)            
    ldr r1, =GPIO_BTN_DN_MSK                                      
    str r1, [r3]                                          
    ldr r3, =DFLT_STATE_STOP      

    cmp r0, r3                                           
    bne downout    

    ldr r6, =DFLT_ALARM_TIME                             
    ldr r0, =message4                                        
    bl  printf                                           
    bl  out    


up:
    @ reset the button
    ldr r3, =(IO_BANK0_BASE+IO_BANK0_INTR2_OFFSET)     
    ldr r1, =GPIO_BTN_UP_MSK                                  
    str r1, [r3]                                          
    ldr r3, =DFLT_STATE_STOP   

    cmp r0, r3                                           
    bne upout   

    ldr r6, =DFLT_ALARM_TIME                             
    ldr r0, =message4                                        
    bl  printf                                           
    bl  out      


turn_off:
    @ turning led off 
    ldr  r3, =(TIMER_BASE+TIMER_INTE_OFFSET)             
    movs r1, #0                                             
    str  r1, [r3]                                          
    ldr  r0, =message3                                         
    bl   printf                                            
    bl   out    


upout:   
    @ increasing rate
    lsrs r6, r6, #1                                        
    ldr  r0, =message5                                        
    bl   printf                                           
    bl   out     


downout:
    @ decreasing rate
    lsls r6, r6, #1                                        
    ldr  r0, =message6                                        
    bl   printf                                           
    bl   out      


out:  

    bl set_alarm  

    pop {pc}                                              
 


@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

@ all the alarm functions

set_alarm:
    @ set
    ldr  r1, =(TIMER_BASE+TIMER_TIMELR_OFFSET)  
    ldr  r3,[r1]                                  
    movs r1, r6                                  
    add  r1, r1, r3                               
    ldr  r3, =(TIMER_BASE+TIMER_ALARM0_OFFSET)  
    str  r1, [r3]                                 
    bx   lr                                       


install_alarm_isr:
    ldr r3, =(PPB_BASE+M0PLUS_VTOR_OFFSET)
    ldr r1, [r3]
    ldr r3, =ALRM_ISR_OFFSET
    adds r3, r1
    ldr r0, =alrm_isr
    str r0, [r3]

    ldr r3, =(PPB_BASE+M0PLUS_NVIC_ICPR_OFFSET)
    movs r0, #1
    str r0, [r3]

    ldr r3, =(PPB_BASE+M0PLUS_NVIC_ISER_OFFSET)
    movs r0, #1
    str r0, [r3]
    
    bx lr



@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@



.thumb_func
alrm_isr:
 
    push {lr}                                    
    ldr  r1, =(TIMER_BASE+TIMER_INTR_OFFSET)     
    movs r0, #1                                 
    str  r0, [r1]                                 
    movs r0, #GPIO_LED_PIN                       
    bl   asm_gpio_get                           
    cmp  r0, #LED_VAL_OFF    @ checking if the led is off          
    beq  led_on     
    
    @ turning the led off
    movs r1, #LED_VAL_OFF                         
    b led_set      


led_on:
    movs r1, #LED_VAL_ON    
    b led_set

led_set:
    movs r0, #GPIO_LED_PIN                      
    bl   asm_gpio_put                           
    bl   set_alarm    
    @ enabling alarm                            
    ldr  r3, =(TIMER_BASE+TIMER_INTE_OFFSET)     
    movs r1, #1                                     
    str  r1, [r3]      
    @ console message for the change in led status                           
    ldr  r0, =message1                              
    bl   printf                                 
    pop  {pc}                                   



@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@



disable_alarm_int:
    movs r1, #0                                                 @ Disable Alarm interupt
    ldr r3, =(TIMER_BASE + TIMER_INTE_OFFSET)                 @ Disable Alarm interupt
    str r1, [r3]                                              @ Disable Alarm interupt

    bx lr
enable_alarm_int:
    movs r1, #1                                       @ Enable Alarm
    ldr r3, =(TIMER_BASE + TIMER_INTE_OFFSET)       @ Enable Alarm
    str r1, [r3]                                   @ Enable Alarm

    bx lr



@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@



.align 4
message1: .asciz "LED changed\n"
message2: .asciz "LED on \n"
message3: .asciz "LED off \n"
message4: .asciz "rate reset \n"
message5: .asciz "rate increased \n"
message6: .asciz "rate decreased \n"

.data
lstate: .word   DFLT_STATE_STRT
ltimer: .word   DFLT_ALARM_TIME
